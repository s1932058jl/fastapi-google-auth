# fastapi google auth

### 設定
Google Cloudにて，OAuth 2.0 クライアント IDを作成する．

また，承認済みのリダイレクト URI に
- http://localhost:8000/
- http://localhost:8000/login/
- http://localhost:8000/auth/

を追加する．

apiフォルダの中に.envファイルを作成して，.env_sampleを参考にGoogle Cloudから以下の2つを取得して，所定の位置に書き込む．
- クライアント ID
- クライアント シークレット

SECRET_KEYはなんでもよい．

### 使い方
設定を終えた前提

```
make run app
```
でコンテナを起動させる．

http://localhost:8000/
にアクセス．