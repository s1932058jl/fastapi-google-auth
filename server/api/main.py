import os

from authlib.integrations.starlette_client import OAuth
from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from starlette.middleware.sessions import SessionMiddleware
from starlette.responses import RedirectResponse

app = FastAPI()
app.add_middleware(SessionMiddleware, secret_key=os.getenv("SECRET_KEY"))

oauth = OAuth()
oauth.register(
    name="google",
    server_metadata_url="https://accounts.google.com/.well-known/openid-configuration",
    client_id=os.getenv("CLIENT_ID"),
    client_secret=os.getenv("CLIENT_SECRET"),
    client_kwargs={
        "scope": "openid email profile",
        "prompt": "select_account",
    },
)


@app.get("/")
async def home(request: Request):
    user = request.session.get("user")
    if user is not None:
        return HTMLResponse(
            f'<body>\
              <h1>Hi! {user["name"]}</h1>\
              <img src="{user["picture"]}" />\
              <a href="/logout">Log out</a>\
            </body>'
        )
    return HTMLResponse('<body><a href="/login">Log In</a></body>')


@app.route("/login/")
async def login_via_google(request: Request):
    google = oauth.create_client("google")
    redirect_uri = request.url_for("authorize_google")
    return await google.authorize_redirect(request, redirect_uri)


@app.route("/auth/")
async def authorize_google(request: Request):
    google = oauth.create_client("google")
    token = await google.authorize_access_token(request)

    user = token.get("userinfo")
    if user:
        request.session["user"] = user
    return RedirectResponse(url="/")


@app.route("/logout")
async def logout(request: Request):
    request.session.pop("user", None)
    return RedirectResponse(url="/")
